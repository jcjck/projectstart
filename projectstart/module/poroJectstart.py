
import jinja2
from jinja2 import Environment, FileSystemLoader
import os
import click
import yaml
import shutil

class Projectstart:
        
        
    def __init__(self,filename,module):
        self.filename=filename
        self.module=module
        self.conf='conf'
        self.templates='templates'
        self.projectpath=self.generate_project_pat(filename=filename,module=module,conf=self.conf,templates=self.templates)

        current_file = __file__
        current_directory = os.path.dirname(current_file)
        
        # 主目录
        self.parent_directory = os.path.abspath(os.path.join(current_directory, ".."))
        
        # yaml文件目录
        self.conffile=self.parent_directory + '\\' +'conf'
        
        # setup.yaml
        self.setupyaml=self.conffile+'setup.yaml'
        # setup.py
        self.setuppy=self.projectpath['projecthome']+'\\setup.py'
        
        #模块路径
        self.libpath=self.projectpath['modulepath']
        
        # conf原文件
        self.confyaml=self.conffile+'\\conf.yaml'
        # conf文件位置保存位置
        self.confpath=self.projectpath['conf']
        
        # templates
        self.templates=self.parent_directory + '\\' +'templates'
        
        # appmanager 保存位置
        self.appmanagerpy=self.projectpath['projecthome']
        
        # appmanager 源文件
        self.appmanagerpy_sorce=self.templates + '\\'+ "appmanage.py"
        
        # setup.html 
        self.setup_html_sorce=self.templates + '\\'+ "setup.html"
        # click.html 
        self.setup_click_sorce=self.templates + '\\'+ "click.html"
        # appmanager html

      
    def generate_appmanagerpy(self):
        # # print(self.appmanagerpy)
        # 源文件路径
        source_file = self.appmanagerpy_sorce
        # 目标文件路径
        destination_file = self.appmanagerpy
        shutil.copy(source_file, destination_file)
        
        
         # 源文件路径
        source_file = self.setup_html_sorce
        destination_file=self.projectpath['templates']
        shutil.copy(source_file, destination_file)
        
        # 源文件路径
        source_file = self.setup_click_sorce
        destination_file=self.projectpath['templates']
        shutil.copy(source_file, destination_file)
        
    def generate_conf(self):
        
        confdata=self.read_yaml(self.confyaml)
        # # print(confdata)
        confdata['appname']=self.filename
        confdata['NAME']=self.filename
        
        confsave=self.confpath+'\\conf.yaml'
        self.wirte_yaml(confsave,confdata)
        return confdata
      
      
    @classmethod  
    def read_yaml(cls,yamlfile):
        '''读取yaml'''
        with open(yamlfile,'r',encoding='utf-8') as confg:
            confdata=confg.read()
            yamldata=yaml.safe_load(confdata)
            return yamldata
    @classmethod
    def wirte_yaml(cls,yamlfile,data):
        '''写入yaml'''
        with open(yamlfile,'w',encoding='utf-8') as confg:
            yamldata=yaml.safe_dump(data)
            confdata=confg.write(yamldata)

            
            
    def create_procjec_files(self):
        # 创建项目需要的目录目录
        # self.projectpath=self.generate_project_pat(filename=filename,module=module )
        self.create_files(self.projectpath)
        
        
        
        
        
    def create_init_files(self):
        '''# 创建包的_init_文件'''
        # 包
        modulefiles=self.get_modules_file(self.projectpath['modulepath'])
        # # print(modulefiles)
        # init内容
        content=self.generate_init_content(modulefiles,libpath=self.module,filename=self.filename)
        # # print(content)
        # 写入init文件
        self.create_initfile(initfile=self.projectpath,content=content)
        
        # # print(modulefiles)
        # # self.create_initfile()
        
        
        
    def create_setup(self):
        templatespath=self.load_templates()
        confdata=self.generate_conf()
        setup_content=self.generate_setup(
            templatespath=templatespath,
            html='setup.html',
            NAME=self.filename,
            VERSION=confdata['VERSION'],
            REQUIRES=confdata['REQUIRES'],
            console_scripts=confdata['console_scripts'],
            keywords=confdata['appname'],
            description=confdata['description'],
            
        )
        # # print(setup_content)
        setup_save_path=self.setup_save_path(setup_save=self.setuppy)
        # # print(setup_save_path)
        self.save_file(setup_save_path,setup_content)
        
        
        
        
    @classmethod
    def save_file(cls,file,content:str):
        # 写入文件
        with open(file,'w',encoding='utf-8') as infile:
            infile.write(content)
   
    @classmethod
    def generate_project_pat(cls,
                             filename,
                             module,
                             conf,
                             templates,
                             ):
        # 项目名称 成项目文件夹
        allpath={}
        workpath=os.getcwd()
        # # print(type(workpath))
        # # print(workpath)
        projectpath=workpath+'\\'+filename+'\\'+filename
        projecthome=workpath+'\\'+filename
        modulepath=projectpath+'\\'+module
        conf=projectpath+'\\'+conf
        templates=projectpath+'\\'+templates
        
        allpath['projectpath']=projectpath
        allpath['modulepath']=modulepath
        allpath['conf']=conf
        allpath['projecthome']=projecthome
        allpath['templates']=templates
        # print(allpath)
        return allpath
    
    
    
    @classmethod
    def create_files(cls,path:dict):
        #  创建目录
        '''
        module
        yaml
        template
        '''
        for filepath in path.values():
            # print('构建文件')
            if  os.path.exists(filepath) == False:
                os.makedirs(filepath)


    
    
    @classmethod
    def get_modules_file(cls,fpath):
        # 获取模块文件
        filelist=[]
        extensfile=['__init__.py']
        for file in os.listdir(fpath):
            file_path = os.path.join(fpath, file)
            if os.path.isfile(file_path):
                # 处理文件
                # 在此处可以进行你想要的操作，例如打印文件名、读取文件内容等
                if file not in extensfile:
                    filelist.append(file.replace('.py', ''))
        return filelist
        
        pass
    @classmethod
    def generate_init_content(cls,modules_file:list,libpath,filename):
        # 生成init文件内容
        content=''
        fromcontent="from {pakpath}.{pakname} import * \n"
        libpath=filename+'.'+libpath
        for i in modules_file:
            fromcontent=fromcontent.format(pakpath=libpath,pakname=i)
            content += fromcontent
        return content
        # print(content)
        
    @classmethod
    def create_initfile(cls,initfile:dict,content:str):
        # 写入文件
        for k in initfile.values():
            with open(k+'\\'+'__init__.py','w',encoding='utf-8') as infile:
                infile.write(content)
        # 构建导入
        
    
    @classmethod
    def load_templates(cls):
        # 加载模板路径
        workpath=os.getcwd()
        # # print(type(workpath))
        # print(workpath)
  
        
        current_file = __file__

        # print(os.getcwd())
        current_directory = os.path.dirname(current_file)
        parent_directory = os.path.abspath(os.path.join(current_directory, ".."))
        templates_path=parent_directory+'\\'+'templates'
        # print(templates_path)
        return templates_path
        
    @classmethod
    def generate_setup(cls,templatespath,html=None,
                        NAME=None,
                        VERSION=None,
                        REQUIRES=[],
                        console_scripts=[],
                        keywords=None,
                        description=None,
                        
                       ):
        '''
        NAME = "poroJectstart" #包名
        VERSION = "1.0.0" # 版本
        REQUIRES = ["six",'pyyaml']  #依赖包
        console_scripts=[]    # 入口文件 . 目录层级 :包内的方法
        description #描述信息
        keywords # 入口文件所在目录
        '''
        env = Environment(loader=FileSystemLoader(templatespath))
        # print(html)
        template = env.get_template(html)
        # NAME=NAME
        # VERSION=obj.version
        # REQUIRES=obj.requires
        # console_scripts=obj.console_scripts
        # keywords=obj.keywords
        # description=obj.description
        template_vars = {"NAME" : NAME,
                        "VERSION": VERSION,
                        "REQUIRES": REQUIRES,
                        "console_scripts": console_scripts,
                        "keywords": NAME,
                        "description": description,
                        
                        
                        }
        html_out = template.render(template_vars)

        return html_out

    @classmethod
    def setup_save_path(cls,setup_save):
        allpath={}
        workpath=os.getcwd()
        # # print(type(workpath))
        
        setup_save=setup_save
        # print(setup_save)
        return setup_save
        pass
    def create_install_file(self):
        # 生成打包安装脚本
        pass
# def main():
#     # Projectstart().create_procjec_files()
#     # Projectstart().create_init_files()
#     # Projectstart().create_setup()
    
    
    

import re

def is_valid_english_input(input_string):
    pattern = r'^[a-zA-Z]*$'
    match = re.match(pattern, input_string)
    return match is not None

# # 获取用户输入
# user_input = input("请输入英文：")

# # 检查输入是否有效
# if is_valid_english_input(user_input):
#     # print("有效的英文输入")
# else:
#     # print("无效的英文输入")
    
@click.group()
def main():
   pass

@main.command()
def create():
    '''创建项目'''
    try:
        
        appname=input("项目名称(全英文字符):")
        if is_valid_english_input(appname):
            pros=Projectstart(appname,'module')
            pros.create_procjec_files()
            pros.create_init_files()
            pros.create_setup()
            pros.generate_conf()
            pros.generate_appmanagerpy()
        else:
            print("无效的输入")
    

    except:
        
        click.echo(f'名称不规范:{appname}')

# @main.command()
# def setup():
#      '''构建项目'''
#      Projectstart().create_init_files()

# @main.command()
# def build():
#      '''打包'''
#      # print('aaa')
#      Projectstart().create_setup()
   

#     # 
    
if __name__ == '__main__':
    main()
    # pros=Projectstart('ttttt','module')
    # pros.create_procjec_files()
    # pros.create_init_files()
    # pros.create_setup()
    # pros.generate_conf()
    # pros.generate_appmanagerpy()
    # # main()
    # pros().create_procjec_files()
    # pros().create_init_files()
    # pros().load_templates()
    # pros().create_setup()
    pass

    



