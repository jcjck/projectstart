# projectstart

#### 介绍
快速创建python包，生成setup.py配置文件，导入相关包，并执行打包安装。

#### 软件架构
软件架构说明


#### 安装教程

1.  使用[install.cmd](install.cmd) 脚本文件快速安装
2.  或者 pip install [dist\projectstart-1.1.1-py3-none-any.whl](dist/projectstart-1.1.1-py3-none-any.whl)

### 项目文件结构
```
park  
  |--park  
  |  |--conf #配置目录
  |  |  |--conf.yaml # 配置文件
  |  |--module #模块目录
  |  |--templates #模板目录
  |  |--appmanage.py 项目管理器
  |  |--setup.py 项目打包配置
```

### 模块文件规范
## module 模块命名规则  
  - 模块命全小写,可使用 `_` 英文下划线。
  - 模块内类名使用模块文件名，首字母大写，其他必须保持一致。
### 模块文件示例
```
class Instassociation_creatse():
    
    '''
    doc:
        创建实例关联关系
    command:
        crate-Instassociation
    Parameters:
        sourec: 
            - 源模型 
            - s
        sourcebind: 
            - 源模型绑定的字段
            - sbd
        dist: 
            - 目标模型 
            - d
        distbind: 
            - 目标模型绑定的字段
            - dbd
        bk_obj_asst_id: 
            - 模型关联关系id
            - boid
    '''
    def __init__(self,sourec=None,sourcebind=None,dist=None,distbind=None,bk_obj_asst_id=None):
      pass
```
- 注释必须写
  - doc 模块功能说明
  - command 模块入口命令，如无需命令行使用可不写
  - Parameters：参数 。采用字典嵌套列表的形式，list[0] 参数说明。list[1] 短参数，代替参数的简写。
   

#### 使用说明

1. 安装完成后进入创建项目的文件夹，执行pyprostart  
![alt text](image/image.png)
2.  创建项目 pyprostart create 会提示输入项目名称，确认后会在当前目录下创建项目文件  
![alt text](image/image-1.png)
![alt text](image/image-2.png)
3. 项目中文件说明
   1. appmanage.py 管理项目 python appmanage
    ![alt text](image/image-4.png)
4.  项目配置文件
![alt text](image/image-5.png)
```
NAME #包名
VERSION 版本
REQUIRES  #依赖包['pyyaml','jinja2','click'] 
console_scripts  # 入口文件 . 目录层级 :包内的方法 ['pyprostart = projectstart.module:main'] 
description  #描述信息
keywords  # 打包文件

```
  > 可自行在网上搜索setup文件参考


5. 模块目录 module  在此目录下编写模块包文件
   
![alt text](image/image-6.png)

> 演示如下：
> ![alt text](image/image-8.png)
> 
> python.exe .\appmanage.py init ,会创建 __init__.py 文件 并导入我们新创建的包
> 
> ![alt text](image/image-9.png)
> ![alt text](image/image-10.png)
>
> python.exe .\appmanage.py setup 对setup.py文件进行更新
>
> ![alt text](image/image-11.png)
> ![alt text](image/image-12.png)
>
> python.exe .\appmanage.py install  打包并且安装程序
> ![alt text](image/image-13.png)
>
>  输入 pip list 可以看到刚刚的包
> ![alt text](image/image-14.png)
>
>  导入使用
>
> ![alt text](image/image-15.png)