# coding: utf-8
from setuptools import setup, find_packages  # noqa: H301
import setuptools

NAME = 'aliyutest' #包名
VERSION ='None' # 版本
REQUIRES =[]  #依赖包
console_scripts= []   # 入口文件 . 目录层级 :包内的方法
description='测试'
keywords='aliyutest'
setup(
    name=NAME,
    version=VERSION,
    description=description,
    author="yjc",
    author_email="yjc@openapitools.org",
    url="",
    keywords=keywords, # 程序所在目录
    install_requires=REQUIRES,
    packages=setuptools.find_packages(),
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ], # 版本
    entry_points={
        'console_scripts':console_scripts   
    }

)