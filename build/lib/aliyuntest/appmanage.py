# 初始化

# 创建更新setup文件


# 打包安装
import os
import yaml
from yamlconfig import config
import jinja2
from jinja2 import Environment, FileSystemLoader
import os
import click
import yaml
class appMnage():
    
    def __init__(self):
        self.filenames=['module','conf','templates']
        self.allpath=self.get_project_path(self.filenames)
        # conf目录
        self.conffilepath=self.allpath['conf']
        # conf位置
        self.conf=self.conffilepath+"\\"+'conf.yaml'
        # 读取配置文件
        self.confdata=self.generate_conf(self.conf)
        print(self.confdata)
        
        # 模板文件目录
        self.templates_path=self.allpath['templates']
        # setup模板
        self.templates_file= self.templates_path+'\\'+'setup.html'
        
        # setup.py
        self.setuppy_file=  self.allpath['workpath'] + '\\' +'setup.py'
        
    def get_project_path(self,namelist:list):
        workpath=os.getcwd()
        homepath=workpath+'\\'+workpath.split('\\').pop()
        allpath={}
        allpath['workpath']=workpath
        allpath['homepath']=homepath
        for i in namelist:
            allpath[i]=homepath+'\\'+i
        return allpath
        
        
        
        
        
    def generate_conf(self,yamlpath):
        confdata=self.read_yaml(yamlpath)
        return confdata   
        
    @classmethod  
    def read_yaml(cls,yamlfile):
        '''读取yaml'''
        with open(yamlfile,'r',encoding='utf-8') as confg:
            confdata=confg.read()
            yamldata=yaml.safe_load(confdata)
            return yamldata
    @classmethod
    def wirte_yaml(cls,yamlfile,data):
        '''写入yaml'''
        with open(yamlfile,'w',encoding='utf-8') as confg:
            yamldata=yaml.safe_dump(data)
            confdata=confg.write(yamldata)
            
            
            
            
    def create_setup(self):
        templatespath=self.templates_path
        confdata=self.confdata
        setup_content=self.generate_setup(
            templatespath=templatespath,
            html='setup.html',
            NAME=self.confdata['NAME'],
            VERSION=confdata['VERSION'],
            REQUIRES=confdata['REQUIRES'],
            console_scripts=confdata['console_scripts'],
            keywords=confdata['appname'],
            description=confdata['description'],
            
        )
        print(setup_content)
        self.save_file(self.setuppy_file,setup_content)
        
    @classmethod
    def save_file(cls,file,content:str):
        # 写入文件
        with open(file,'w',encoding='utf-8') as infile:
            infile.write(content)
        
    @classmethod
    def generate_setup(cls,templatespath,html=None,
                        NAME=None,
                        VERSION=None,
                        REQUIRES=[],
                        console_scripts=[],
                        keywords=None,
                        description=None,
                        
                       ):
        '''
        NAME = "poroJectstart" #包名
        VERSION = "1.0.0" # 版本
        REQUIRES = ["six",'pyyaml']  #依赖包
        console_scripts=[]    # 入口文件 . 目录层级 :包内的方法
        description #描述信息
        keywords # 入口文件所在目录
        '''
        env = Environment(loader=FileSystemLoader(templatespath))
        print(html)
        template = env.get_template(html)
        # NAME=NAME
        # VERSION=obj.version
        # REQUIRES=obj.requires
        # console_scripts=obj.console_scripts
        # keywords=obj.keywords
        # description=obj.description
        template_vars = {"NAME" : NAME,
                        "VERSION": VERSION,
                        "REQUIRES": REQUIRES,
                        "console_scripts": console_scripts,
                        "keywords": NAME,
                        "description": description,
                        
                        
                        }
        html_out = template.render(template_vars)

        return html_out









        
    def create_init_files(self):
        '''# 创建包的_init_文件'''
        # 包

        fpath=self.allpath['module']
        homepath=self.allpath['homepath']

        
        modulefiles=self.get_modules_file(fpath=fpath)

        content=self.generate_init_content(modules_file=modulefiles,filename=self.confdata['NAME'])
        print(content)
        # 写入init文件
        projectpath={}
        projectpath['homepath']=homepath
        projectpath['module']=fpath
        self.create_initfile(initfile=projectpath,content=content)
        
        # print(modulefiles)
        # # self.create_initfile()
        
    @classmethod
    def get_modules_file(cls,fpath):
        # 获取模块文件
        '''
        fpath: 绝对路径list
        '''
        filelist=[]
        extensfile=['__init__.py']
        print(fpath)
        for file in os.listdir(fpath):
            file_path = os.path.join(fpath, file)
            if os.path.isfile(file_path):
                # 处理文件
                # 在此处可以进行你想要的操作，例如打印文件名、读取文件内容等
                if file not in extensfile:
                    filelist.append(file.replace('.py', ''))
        return filelist
    
    @classmethod
    def generate_init_content(cls,modules_file:list,filename):
        # 生成init文件内容
        content=''
        fromcontent="from {pakpath}.{pakname} import * \n"
        libpath=filename+'.'+'module'
        for i in modules_file:
            c=fromcontent.format(pakpath=libpath,pakname=i)
            print(c)
            content += c
        return content
    
    @classmethod
    def create_initfile(cls,initfile:dict,content:str):
        # 写入文件
        for k in initfile.values():
            with open(k+'\\'+'__init__.py','w',encoding='utf-8') as infile:
                infile.write(content)
        # 构建导入
        
        
    
    
        
        
        
        
        
        
        
        
        
    
    
if __name__ == "__main__":
    t=appMnage()
    t.create_setup()
    t.create_init_files()
    