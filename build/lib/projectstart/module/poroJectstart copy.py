from yamlconfig import config
import jinja2
from jinja2 import Environment, FileSystemLoader
import os
import click
# obj = config(file='auth')
obj = config()
# obj.appname='create'
obj=obj.reade(file='appconf')
print(dir(obj))
# obj.modulepath='module'
# obj.templates='templates'

# obj.version='1.1.1'
# obj.requires=['dsdsdssddddddssd']
# obj.console_scripts=[]
# obj.description='描述信息'
# obj.keywords=obj.appname

print(dir(obj))

# obj.write()
# obj.test=123
# print(obj.data)


print(obj.appname)

class Projectstart:
        
        
    def __init__(self):
        self.projectpath=self.generate_project_pat()
        current_file = __file__
        current_directory = os.path.dirname(current_file)
        # 主目录
        self.parent_directory = os.path.abspath(os.path.join(current_directory, ".."))
        # yaml文件目录
        self.conffile=self.parent_directory + '\\' +'conf'
        
        # setup.yaml
        self.setupyaml=self.conffile+'setup.yaml'
        #
        
    def create_procjec_files(self):
        # 创建项目需要的目录目录
        self.create_files(self.projectpath)
        
    def create_init_files(self):
        '''# 创建包的_init_文件'''
        # 包
        modulefiles=self.get_modules_file(self.projectpath['modulepath'])
        
        # init内容
        content=self.generate_init_content(modulefiles)
        
        # 写入init文件
        self.create_initfile(initfile=self.projectpath,content=content)
        
        print(modulefiles)
        # self.create_initfile()
    def create_setup(self):
        templatespath=self.load_templates()
        setup_content=self.generate_setup(
            templatespath=templatespath,
            html='setup.html'
        )
        print(setup_content)
        setup_save_path=self.setup_save_path()
        print(setup_save_path)
        self.save_file(setup_save_path,setup_content)
        
        
        
        
    @classmethod
    def save_file(cls,file,content:str):
        # 写入文件
        with open(file,'w',encoding='utf-8') as infile:
            infile.write(content)
   
    @classmethod
    def generate_project_pat(cls):
        # 项目名称 成项目文件夹
        allpath={}
        workpath=os.getcwd()
        # print(type(workpath))
        print(workpath)
        projectpath=workpath+'\\'+obj.appname+'\\'+obj.appname
        modulepath=projectpath+'\\'+obj.modulepath
        
        allpath['projectpath']=projectpath
        allpath['modulepath']=modulepath
        print(allpath)
        return allpath
    @classmethod
    def create_files(cls,path:dict):
        #  创建目录
        '''
        module
        yaml
        template
        '''
        for filepath in path.values():
            print('构建文件')
            if  os.path.exists(filepath) == False:
                os.makedirs(filepath)


    
    
    @classmethod
    def get_modules_file(cls,fpath):
        # 获取模块文件
        filelist=[]
        extensfile=['__init__.py']
        for file in os.listdir(fpath):
            file_path = os.path.join(fpath, file)
            if os.path.isfile(file_path):
                # 处理文件
                # 在此处可以进行你想要的操作，例如打印文件名、读取文件内容等
                if file not in extensfile:
                    filelist.append(file.replace('.py', ''))
        return filelist
        
        pass
    @classmethod
    def generate_init_content(cls,modules_file:list):
        # 生成init文件内容
        content=''
        fromcontent="from {pakpath}.{pakname} import * \n"
        libpath=obj.appname+'.'+obj.modulepath
        for i in modules_file:
            fromcontent=fromcontent.format(pakpath=libpath,pakname=i)
            content += fromcontent
        return content
        print(content)
        
    @classmethod
    def create_initfile(cls,initfile:dict,content:str):
        # 写入文件
        obj.modulepath
        for k in initfile.values():
            with open(k+'\\'+'__init__.py','w',encoding='utf-8') as infile:
                infile.write(content)
        # 构建导入
        
    
    @classmethod
    def load_templates(cls):
        # 加载模板路径
        workpath=os.getcwd()
        # print(type(workpath))
        print(workpath)
  
        
        current_file = __file__

        print(os.getcwd())
        current_directory = os.path.dirname(current_file)
        parent_directory = os.path.abspath(os.path.join(current_directory, ".."))
        templates_path=parent_directory+'\\'+'templates'
        print(templates_path)
        return templates_path
        
    @classmethod
    def generate_setup(cls,templatespath,html=None,
                        NAME=None,
                        VERSION=None,
                        REQUIRES=[],
                        console_scripts=[],
                        keywords=None,
                        description=None,
                        
                       ):
        '''
        NAME = "poroJectstart" #包名
        VERSION = "1.0.0" # 版本
        REQUIRES = ["six",'pyyaml']  #依赖包
        console_scripts=[]    # 入口文件 . 目录层级 :包内的方法
        description #描述信息
        keywords # 入口文件所在目录
        '''
        env = Environment(loader=FileSystemLoader(templatespath))
        print(html)
        template = env.get_template(html)
        NAME=obj.appname
        VERSION=obj.version
        REQUIRES=obj.requires
        console_scripts=obj.console_scripts
        keywords=obj.keywords
        description=obj.description
        template_vars = {"NAME" : NAME,
                        "VERSION": VERSION,
                        "REQUIRES": REQUIRES,
                        "console_scripts": console_scripts,
                        "keywords": keywords,
                        "description": description,
                        
                        
                        }
        html_out = template.render(template_vars)

        return html_out

    @classmethod
    def setup_save_path(cls):
        allpath={}
        workpath=os.getcwd()
        # print(type(workpath))
        
        setup_save=workpath+'\\'+obj.appname+'\\setup.py'
        print(setup_save)
        return setup_save
        pass
    def create_install_file(self):
        # 生成打包安装脚本
        pass
# def main():
#     # Projectstart().create_procjec_files()
#     # Projectstart().create_init_files()
#     # Projectstart().create_setup()
    
    
    

import re

def is_valid_english_input(input_string):
    pattern = r'^[a-zA-Z]*$'
    match = re.match(pattern, input_string)
    return match is not None

# # 获取用户输入
# user_input = input("请输入英文：")

# # 检查输入是否有效
# if is_valid_english_input(user_input):
#     print("有效的英文输入")
# else:
#     print("无效的英文输入")
    
@click.group()
def main():
   pass

@main.command()



def create():
    '''创建项目'''
    try:
        
        appname=input("项目名称(全英文字符):")
        if is_valid_english_input(appname):
            obj.appname=appname
            obj.modulepath='module'
            obj.templates='templates'
            Projectstart().create_procjec_files()
            Projectstart().create_init_files()
            Projectstart().create_setup()
            click.echo(f'项目名称:{appname}')
        else:
            print("无效的输入")
    

    except:
        click.echo(f'名称不规范:{appname}')

@main.command()
def setup():
     '''构建项目'''
     Projectstart().create_init_files()

@main.command()
def build():
     '''打包'''
     print('aaa')
     Projectstart().create_setup()
   

    # 
    
    

if __name__ == '__main__':
    main()
    # Projectstart().create_procjec_files()
    # Projectstart().create_init_files()
    # Projectstart().load_templates()
    # Projectstart().create_setup()


